# Vcode Files

Needed settings for vscode

## launch.json

    {
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Chrome",
            "type": "chrome",
            "request": "launch",
            "url": "http://localhost:3000",
            "webRoot": "${workspaceRoot}",
            "sourceMaps": true,
            "trace": true,
            "sourceMapPathOverrides": {
                "webpack:///./*": "${webRoot}/*"
            }
        }
    ]
}

## settings.json

    {
    "files.exclude": {
        "node_modules/": true,
        "dist/": true,
        "lib/": true,
        "typings/": true
    },
    "javascript.validate.enable": false,
    "eslint.autoFixOnSave": true,
    "javascript.implicitProjectConfig.checkJs": true,
    "tslint.autoFixOnSave": true,
    "typescript.reportStyleChecksAsWarnings": false,
}

## scripts to start
    npm i
    npm start
    f5 in VScode

## .env
    PAGE_SERVER_SCHEMA=http
    PAGE_SERVER_HOST=localhost
    PAGE_SERVER_PORT=3000

    API_SERVER_SCHEMA=http
    API_SERVER_HOST=localhost
    API_SERVER_PORT=3001
    
## How to run translate builder
```$xslt
npm run intl:compile
npm run intl:update
```
