import * as React from "react";

import {State} from "./types";

class ErrorBoundary extends React.Component<{}, State> {
  state = {
    hasError: false,
  };

  static getDerivedStateFromError() {
    return {hasError: true};
  }

  componentDidCatch(error: object, info: object) {
    console.log("Error", error, info);
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
