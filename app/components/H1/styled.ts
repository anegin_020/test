import styled from "styled-components";

export const H1 = styled.h1`
  margin: 0 0 2em;
  font-size: 36px;
`;
