import { API_SERVER_CONFIG } from "./apiServerConfig";

const apiServer = `${API_SERVER_CONFIG.API_SERVER_SCHEMA}://${API_SERVER_CONFIG.API_SERVER_HOST}:${API_SERVER_CONFIG.API_SERVER_PORT}`;

export const SERVER_ADDRESSES = {
    DEFAULT: apiServer,
    GENERATE_CSV_SERVER: "todo, make real",
};
