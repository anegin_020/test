import Redux, { createStore, applyMiddleware } from "redux";
import { routerMiddleware } from "react-router-redux";
import createSagaMiddleware from "redux-saga";
import { composeWithDevTools } from "redux-devtools-extension";
import { fromJS } from "immutable";

import createReducerImported from "./reducers";
import { BaseStoreProps } from "utils/types";

let createReducer = createReducerImported;

const sagaMiddleware = createSagaMiddleware();

export default <T>(initialState: object = {}, history: any): BaseStoreProps<T> => {
  const middlewares: Redux.Middleware[] = [
    routerMiddleware(history),
    sagaMiddleware
  ];

  const composeEnhancers = composeWithDevTools({});

  const store: BaseStoreProps<T> = createStore(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(applyMiddleware(...middlewares)),
  );

  store.runSaga = sagaMiddleware.run;
  store.injectedReducers = {};
  store.injectedSagas = {};

  if (module.hot) {
    module.hot.accept("./reducers", () => {
      console.log("here");
      import("./reducers").then((result) => {
        createReducer = result.default;
        store.replaceReducer(createReducer(store.injectedReducers));
      });
    });
  }

  return store;
};
