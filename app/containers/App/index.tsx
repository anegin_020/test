import React from "react";
import { Switch, Route, Link } from "react-router-dom";

import {AppProps} from "./types";
import HomePage from "containers/HomePage/Loadable";
import H1 from "components/H1";

export class App extends React.Component<AppProps, {}> {
  render() {
    return (
      <div>
        <H1>Hello from {this.props.compiler} and {this.props.framework}!</H1>
        <Link to="/">Go Home</Link><br/>
        <Link to="/other">Go to not found</Link><br/>
        <Switch>
          <Route exact path="/" component={HomePage}/>
        </Switch>
      </div>
    );
  }
}

export default App;



