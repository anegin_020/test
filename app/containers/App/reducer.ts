import { fromJS } from "immutable";

import { BaseActionProps } from "utils/types";

const initialState = fromJS({});

function appReducer(state = initialState, action: BaseActionProps) {
  switch (action.type) {
    default:
      return state;
  }
}

export default appReducer;
