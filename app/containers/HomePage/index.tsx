import * as React from "react";
import { connect } from "react-redux";
import { compose, Dispatch } from "redux";
import { createStructuredSelector } from "reselect";
import { FormattedMessage } from "react-intl";

import reducer from "./reducer";
import saga from "./sagas";

import injectReducer from "../../utils/ijectModule/injectReducer";
import injectSaga from "../../utils/ijectModule/injectSaga";

import { Props, DispatchProps } from "./types";
import { changeLocale } from "containers/LocaleProvider/actions";
import { selectName } from "./selectors";
import messages from "./messages";
import { makeSelectLocale } from "../LocaleProvider/selectors";

class HomePage extends React.PureComponent<Props> {
  handleClick = () => {
    this.props.changeLocale(this.props.locale === "en" ? "ru" : "en");
  }

  render() {
    const name = this.props.name;

    return (
      <div>
        <h2>
          {name}:
          <FormattedMessage {...messages.header} />
        </h2>
        <br/>
        <p>
          <FormattedMessage {...messages.content} />
        </p>
        <button onClick={this.handleClick}>
          Change locale
        </button>
        <br/>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  name: selectName(),
  locale: makeSelectLocale(),
});

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  changeLocale: (locale) => dispatch(changeLocale(locale))
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({key: "home", reducer});
const withSaga = injectSaga({key: "home", saga});

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
