import {defineMessages} from "react-intl";

export const scope = "app.containers.HomePage";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the instagramm's catalog",
  },
  content: {
    id: `${scope}.content`,
    defaultMessage: "This is the content for instagramm's catalog",
  },
  footer: {
    id: `${scope}.content`,
    defaultMessage: "This is the content for instagramm's catalog",
  },
});
