import { fromJS } from "immutable";

import { BaseActionProps } from "utils/types";

const initialState = fromJS({
  name: "HOME PAGE",
});

function homeReducer(state = initialState, action: BaseActionProps) {
  switch (action.type) {
    default:
      return state;
  }
}

export default homeReducer;
