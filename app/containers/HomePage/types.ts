import {ChangeLocaleDispatchProps, appLocalesType} from "containers/LocaleProvider/types";

export interface HomePageProps {
  name: string;
  locale: appLocalesType;
}

export type Props = HomePageProps & ChangeLocaleDispatchProps;

export type DispatchProps = ChangeLocaleDispatchProps;
