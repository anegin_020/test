import { BaseActionProps } from "utils/types";
import { appLocalesType } from "utils/i18n/types";
import { CHANGE_LOCALE } from "./constants";

export function changeLocale(languageLocale: appLocalesType): BaseActionProps {
  return {
    type: CHANGE_LOCALE,
    payload: languageLocale,
  };
}
