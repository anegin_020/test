import React from "react";
import { connect } from "react-redux";
import { createSelector } from "reselect";
import { IntlProvider } from "react-intl";
import { isEmpty } from "lodash";

import { DEFAULT_LOCALE } from "utils/i18n/constants";
import { LocaleProviderState, LocaleProviderProps, appLocalesType } from "./types";
import { makeSelectLocale } from "./selectors";

export class LanguageProvider extends React.Component<LocaleProviderProps, LocaleProviderState> {

  constructor(props: LocaleProviderProps) {
    super(props);
    this.state = {
      activeLocale: props.locale,
    };
  }

  shouldComponentUpdate(nextProps: Readonly<LocaleProviderProps>): boolean {
    if (this.props.locale !== nextProps.locale) {
      this.handleChangeLocale(nextProps.locale);
      return false;
    }
    return true;
  }

  handleChangeLocale = (locale: appLocalesType) => {
    if (isEmpty(this.props.localeStore.getLocale(locale))) {
      this.props.localeStore.loadLocale(locale)
        .then(() => {
          this.setState(() => ({
            activeLocale: locale,
          }));
        });
    } else {
      this.setState(() => ({
        activeLocale: locale,
      }));
    }
  }

  render() {
    const {
      props: {localeStore, children},
      state: {activeLocale},
    } = this;

    return (
      <IntlProvider
        defaultLocale={DEFAULT_LOCALE}
        locale={activeLocale}
        key={activeLocale}
        messages={localeStore.getLocale(activeLocale)}
      >
        {React.Children.only(children)}
      </IntlProvider>
    );
  }
}

const mapStateToProps = createSelector(makeSelectLocale(), locale => ({
  locale,
}));

export default connect(mapStateToProps)(LanguageProvider);
