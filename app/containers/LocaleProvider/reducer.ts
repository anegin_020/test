import { fromJS } from "immutable";

import { BaseActionProps } from "utils/types";
import { CHANGE_LOCALE } from "./constants";
import { DEFAULT_LOCALE } from "utils/i18n";

export const initialState = fromJS({
  locale: DEFAULT_LOCALE,
});

function languageProviderReducer(state = initialState, action: BaseActionProps) {
  switch (action.type) {
    case CHANGE_LOCALE:
      return state.set("locale", action.payload);
    default:
      return state;
  }
}

export default languageProviderReducer;
