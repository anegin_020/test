import {LocalStorageProps, appLocalesType} from "utils/i18n/types";

interface LocaleProviderState {
  activeLocale: appLocalesType;
}

interface LocaleProviderProps {
  locale?: appLocalesType;
  localeStore: LocalStorageProps;
}

interface ChangeLocaleDispatchProps {
  changeLocale: (locale: appLocalesType) => any;
}

export {
  appLocalesType,
  LocaleProviderProps,
  LocaleProviderState,
  ChangeLocaleDispatchProps,
};
