import "@babel/polyfill";

import * as React from "react";
import * as ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { createBrowserHistory } from "history";

import "config/typescript";
import LanguageProvider from "containers/LocaleProvider";
import AppImport from "./containers/App";
import rootSaga from "./containers/App/sagas";

import configureStore from "./configureStore";

import { initLocaleStore, DEFAULT_LOCALE } from "utils/i18n";

const MOUNT_NODE = document.getElementById("app");

const history = createBrowserHistory();

const initialState = {};
const store = configureStore(initialState, history);
const localeStore = initLocaleStore();

store.runSaga(rootSaga);
let App = AppImport;

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <LanguageProvider localeStore={localeStore}>
        <ConnectedRouter history={history}>
          <App compiler="TypeScript" framework="React"/>
        </ConnectedRouter>
      </LanguageProvider>
    </Provider>,
    MOUNT_NODE
  );
};

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(["./containers/App"], () => {
    import("./containers/App").then((result) => {
      App = result.default;
      ReactDOM.unmountComponentAtNode(MOUNT_NODE);
      render();
    });

  });
}

/*
В документации по react-intel ничего не говорится про подключение "intl"

// @ts-ignore
if (!window.Intl) {
  new Promise(resolve => {
    // @ts-ignore
    resolve(import("intl"));
  })
    .then(() =>
      Promise.all([
        // @ts-ignore
        import("intl/locale-data/jsonp/en.js"),
        // @ts-ignore
        import("intl/locale-data/jsonp/ru.js"),
      ]),
    )
    .then(() => render(translationMessages))
    .catch(err => {
      throw err;
    });
} else {
  render(translationMessages);
}*/

localeStore.loadLocale(DEFAULT_LOCALE).then(() => {
  render();
});

if (process.env.NODE_ENV === "production") {
  require("offline-plugin/runtime").install(); // eslint-disable-line global-require
}
