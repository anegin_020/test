import Redux from "redux";
import { fromJS } from "immutable";
import { combineReducers } from "redux-immutable";
import { LOCATION_CHANGE } from "react-router-redux";

import globalReducer from "containers/App/reducer";
import languageProviderReducer from "containers/LocaleProvider/reducer";

import { BaseActionProps } from "utils/types";

const routeInitialState = fromJS({
  locationBeforeTransitions: null,
});

function routeReducer(state = routeInitialState, action: BaseActionProps) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return state.merge({
        locationBeforeTransitions: action.payload,
      });
    default:
      return state;
  }
}

export default (injectedReducers: Redux.ReducersMapObject = {}): any => {
  return combineReducers({
    route: routeReducer,
    global: globalReducer,
    language: languageProviderReducer,
    ...injectedReducers,
  });
};
