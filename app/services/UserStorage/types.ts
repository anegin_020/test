import { AnyObjectType } from "utils/types/primitiveData";

export interface UserStorageInterface {
  get: (key: string) => AnyObjectType;
  set: (key: string, data: any) => void;
  remove: (key: string) => void;
  getAll: () => AnyObjectType;
}
