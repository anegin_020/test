import { conformsTo, isFunction, isObject } from "lodash";
import invariant from "invariant";

import { BaseStoreProps } from "utils/types";

/**
 * Validate the shape of redux store
 */
export default function checkStore<T>(store: BaseStoreProps<T>) {
  const shape = {
    dispatch: isFunction,
    subscribe: isFunction,
    getState: isFunction,
    replaceReducer: isFunction,
    runSaga: isFunction,
    injectedReducers: isObject,
    injectedSagas: isObject,
  };

  invariant(
    // @ts-ignore
    conformsTo(store, shape),
    "(app/utils...) injectors: Expected a valid redux store"
  );
}
