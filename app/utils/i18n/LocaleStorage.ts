import { LocalStorageProps, appLocalesType } from "./types";
import { DEFAULT_LOCALE } from "./constants";

export default (function (): LocalStorageProps {
  const _state: {[index: string]: {}} = {};

  const formatTranslationMessages = (
    locale: appLocalesType,
    messages: { [index: string]: { message: string } }
  ): { [index: string]: { message: string } } => {
    const defaultFormattedMessages = locale !== DEFAULT_LOCALE
      ? formatTranslationMessages(DEFAULT_LOCALE, _state[DEFAULT_LOCALE])
      : {};

    const flattenFormattedMessages = (formattedMessages: string, key: string) => {
      const formattedMessage = !messages[key] && locale !== DEFAULT_LOCALE
        ? defaultFormattedMessages[key]
        : messages[key];
      return Object.assign(formattedMessages, {[key]: formattedMessage});
    };

    return Object.keys(messages).reduce(flattenFormattedMessages, {});
  };

  return {
    loadLocale: (locale: appLocalesType) => {
      return new Promise(resolve => {
        switch (locale) {
          case "en":
            resolve(import("./locales/en"));
            break;
          case "ru":
            resolve(import("./locales/ru"));
            break;
          default: {
            throw `Can't load ${locale} locales!`;
          }
        }
      })
        .then(({default: messages}) => {
          _state[locale] = formatTranslationMessages(locale, messages);
          return this;
        });
    },
    getLocale: (locale: appLocalesType) => {
      return _state[locale];
    },
  };
})();
