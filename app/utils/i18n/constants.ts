import { appLocalesType } from "./types";

// @ts-ignore
export const DEFAULT_LOCALE = (window && window.locale) || "en";

export const appLocales: appLocalesType[] = [
  "en",
  "ru",
];
