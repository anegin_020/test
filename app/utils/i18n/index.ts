import { LocalStorageProps } from "./types";
import { DEFAULT_LOCALE } from "./constants";
import LocaleStorage from "./LocaleStorage";

const initLocaleStore = (): LocalStorageProps => LocaleStorage;

export {
  DEFAULT_LOCALE,
  initLocaleStore,
};
