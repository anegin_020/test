import { addLocaleData } from "react-intl";
import enLocaleData from "react-intl/locale-data/en";

const enTranslationMessages = require("translations/en.json");

addLocaleData(enLocaleData);

export default enTranslationMessages;
