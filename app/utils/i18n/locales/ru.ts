import { addLocaleData } from "react-intl";
import ruLocaleData from "react-intl/locale-data/ru";

const enTranslationMessages = require("translations/ru.json");

addLocaleData(ruLocaleData);

export default enTranslationMessages;

