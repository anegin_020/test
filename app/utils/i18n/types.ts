export type appLocalesType = "en" | "ru";

export interface LocalStorageProps {
  loadLocale?: (locale: appLocalesType) => Promise<{}>;
  getLocale?: (locale: appLocalesType) => {};
}
