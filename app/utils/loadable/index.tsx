import React, { lazy, Suspense } from "react";

import { ImportFuncType, OptionsType } from "./types";

import ErrorBoundary from "components/ErrorBoundary";

const loadable = (
  importFunc: ImportFuncType,
  {fallback = null}: OptionsType = {fallback: null},
): React.ComponentType => {
  const LazyComponent = lazy(importFunc);

  return props => (
    <ErrorBoundary>
      <Suspense fallback={fallback}>
        <LazyComponent {...props} />
      </Suspense>
    </ErrorBoundary>
  );
};

export default loadable;
