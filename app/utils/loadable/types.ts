import React from "react";

export type ImportFuncType = () => Promise<{ default: React.ComponentType }>;

export type OptionsType = { fallback: JSX.Element | null };
