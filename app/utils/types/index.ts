import Redux from "redux";
import { Dispatch } from "redux";
import { SagaIterator, Task } from "redux-saga";

export interface BaseActionProps {
  type: string;
  payload?: any;
  error?: any;
  response?: any;
  meta?: any;
}

export interface BaseStoreProps<T> extends Redux.Store<T> {
  runSaga?: (saga: (...args: any[]) => SagaIterator, ...args: any[]) => Task;
  injectedReducers?: Redux.ReducersMapObject;

  getState(): any;

  injectedSagas?: any;
  replaceReducer: any;
}

export interface BaseDispatchProps {
  dispatch: Dispatch;
}
