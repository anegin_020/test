export type AnyObjectType = { [s: string]: any; };

export type StringObjectType = { [s: string]: string; };

export type NumberObjectType = { [s: string]: number; };
