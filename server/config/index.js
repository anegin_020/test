require('dotenv').config();

module.exports = {
  ENV_CONSTANTS: {
    SERVER_SCHEMA: process.env.SERVER_SCHEMA,
    SERVER_HOST: process.env.SERVER_HOST,
    SERVER_PORT: process.env.SERVER_PORT,
    API_SERVER_SCHEMA: process.env.API_SERVER_SCHEMA,
    API_SERVER_HOST: process.env.API_SERVER_HOST,
    API_SERVER_PORT: process.env.API_SERVER_PORT,
  },
};
