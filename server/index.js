/* eslint-disable global-require */

const chalk = require('chalk');
const Express = require('express');

const setup = require('./middlewares/frontendMiddleware');

const { ENV_CONSTANTS } = require('./config');

const schema = ENV_CONSTANTS.SERVER_SCHEMA;
const port = ENV_CONSTANTS.SERVER_PORT || 3000;
const host = ENV_CONSTANTS.SERVER_HOST || 'localhost';
const app = new Express();

setup(app);

if (schema === 'https') {
  // const https = require('https');
  // const privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');
  // const certificate = fs.readFileSync('sslcert/server.crt', 'utf8');

  // const credentials = {key: privateKey, cert: certificate};
  // const httpsServer = https.createServer(credentials, app);
  //
  // httpsServer.listen(8443, (err) => {
  //   if (err) {
  //     console.log('PAGE SERVER START ERRROR: ', err);
  //   }
  //   console.log(`PAGE SERVER MESSAGE | Page server started on ${host}:${port}`);
  // });
} else {
  const http = require('http');

  const httpServer = http.createServer(app);

  httpServer.listen(+port, host, (err) => {
    if (err) {
      console.log('PAGE SERVER START ERROR: ', err);
    } else {
      console.log(`
      Server started! ${chalk.green('✓')}
      Access URL: ${chalk.magenta(`${schema}://${host}:${port}`)}
      `);
    }
  });
}
