const Webpack = require('webpack');
const WebpackDevMiddleware = require('webpack-dev-middleware');
const WebpackHotMiddleware = require('webpack-hot-middleware');
const Path = require('path');

module.exports = (app, config) => {
  const compiler = Webpack(config);

  const middleware = WebpackDevMiddleware(compiler, {
    logLevel: 'warn',
    publicPath: config.output.publicPath,
    stats: 'errors-only',
  });

  app.use(middleware);
  app.use(WebpackHotMiddleware(compiler, {
    heartbeat: 2000,
  }));

  const fs = middleware.fileSystem;

  app.get('*', (req, res) => {
    fs.readFile(Path.join(compiler.outputPath, 'index.html'), (err, file) => {
      if (err) {
        res.sendStatus(404);
      } else {
        res.send(file.toString());
      }
    });
  });
};
