/* eslint-disable global-require */

module.exports = (app) => {
  const addDevMiddlewares = require('./addDevMiddlewares');
  const config = require('../../internals/webpack/webpack.dev.babel');
  // const isProduction = process.env.NODE_ENV === 'production';

  addDevMiddlewares(app, config);
};
